I want to learn more programming languages.

So, I will go and try to quickly write the same small project on different languages.

The original code is from my book "Boxes": https://ralsina.gitlab.io/boxes-book/

So far:

* Nim: [Blog post](http://ralsina.me/weblog/posts/playing-with-nim.html) and [code](nim)
